/*
 * File      : main.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2017, RT-Thread Development Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Change Logs:
 * Date           Author       Notes
 * 2017-5-30      Bernard      the first version
 */

#include "rtthread.h"
#include "drv_clock.h"
#include <lvgl.h>
#include "custom/ui.h"
#include "drv_touch.h"
#include "lv_port_disp.h"
#include "cJSON.h"

int main(int argc, char **argv){
    rt_kprintf("periph_get_pll_clk:%d\n", periph_get_pll_clk());
    rt_kprintf("cpu_get_clk:%d\n", cpu_get_clk());
    rt_kprintf("ahb_get_clk:%d\n", ahb_get_clk());
    rt_kprintf("apb_get_clk:%d\n", apb_get_clk());
    return 0;
}


static void lvgl_thread(void *parameter){
    lv_init();
    lv_port_disp_init();
    ui_init();
    rt_kprintf("\n ui_init \n");
    /*************JSON 测试 ***********************
    // #include "cJSON.h" // 引用 头文件
    char txt[] = "{\"phone\":\"88888888\",\"email\":\"8888@163.com\",\"current\":1,\"menus\":[{\"id\":1,\"name\":\"烤鸭\",\"img\":\"ui_img_chicken_png\",\"temp\":\"123℃\",\"time\":\"120\",\"humidity\":\"95%\"},{\"id\":2,\"name\":\"烤鸭\",\"img\":\"ui_img_chicken_png\",\"temp\":\"123℃\",\"time\":\"120\",\"humidity\":\"95%\"},{\"id\":3,\"name\":\"烤鸭\",\"img\":\"ui_img_chicken_png\",\"temp\":\"123℃\",\"time\":\"120\",\"humidity\":\"95%\"},{\"id\":4,\"name\":\"烤鸭\",\"img\":\"ui_img_chicken_png\",\"temp\":\"123℃\",\"time\":\"120\",\"humidity\":\"95%\"},{\"id\":5,\"name\":\"烤鸭\",\"img\":\"ui_img_chicken_png\",\"temp\":\"123℃\",\"time\":\"120\",\"humidity\":\"95%\"},{\"id\":6,\"name\":\"烤鸭\",\"img\":\"ui_img_chicken_png\",\"temp\":\"123℃\",\"time\":\"120\",\"humidity\":\"95%\"},{\"id\":7,\"name\":\"烤鸭\",\"img\":\"ui_img_chicken_png\",\"temp\":\"123℃\",\"time\":\"120\",\"humidity\":\"95%\"},{\"id\":8,\"name\":\"烤鸭\",\"img\":\"ui_img_chicken_png\",\"temp\":\"123℃\",\"time\":\"120\",\"humidity\":\"95%\"}]}";
    cJSON *root = cJSON_Parse(txt);
    if(root){
        rt_kprintf("JSON Parse success , print start .......... \n");

        cJSON *phone = cJSON_GetObjectItem(root, "phone");
        if (phone != NULL) {
            rt_kprintf("phone: %s\n", phone->valuestring);
        }
        // Get 设置邮箱
        cJSON *email = cJSON_GetObjectItem(root, "email");
        if (email != NULL) {
            rt_kprintf("email: %s\n", email->valuestring);
        }

        // Get 当前使用的菜单ID
        cJSON *current = cJSON_GetObjectItem(root, "password");
        if (current != NULL) {
            rt_kprintf("current: %d\n", current->valueint);
        }

        // 获取菜单
        cJSON *menus = cJSON_GetObjectItem(root, "menus");
        if (menus != NULL) {
                //if (cJSON_IsArray(menus)){
                int array_size = cJSON_GetArraySize(menus);
                rt_kprintf("array_size: %d\n", array_size);
                cJSON *item; cJSON *id;cJSON *name;cJSON *temp;
                for(int i=0; i< array_size; i++) {
                    item = cJSON_GetArrayItem(root, i);

                    id = cJSON_GetObjectItem(item, "id");
                    if (id != NULL) {
                        rt_kprintf("id: %s\n", id->valueint);
                    }
                    name = cJSON_GetObjectItem(item, "name");
                    if (name != NULL) {
                        rt_kprintf("name: %s\n", name->valuestring);
                    }
                    temp = cJSON_GetObjectItem(item, "temp");
                    if (temp != NULL) {
                        rt_kprintf("temp: %s\n", temp->valuestring);
                    }
                //}
                }
        }
    }else{
        rt_kprintf("JSON parse error \n");
    }
    */

    while (1){
        lv_task_handler();
        rt_thread_mdelay(1);
    }
}

static int lvgl_demo_init(void){
    rt_thread_t tid;
    tid = rt_thread_create("LVGL", lvgl_thread, RT_NULL, 4096,10, 10);
    if (tid == RT_NULL){
        rt_kprintf("Fail to create 'LVGL' thread\n");
    }
    rt_thread_startup(tid);
    return 0;
}
INIT_APP_EXPORT(lvgl_demo_init);

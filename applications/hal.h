/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2023-05-30     Administrator       the first version
 */
#ifndef APPLICATIONS_HAL_H_
#define APPLICATIONS_HAL_H_

void beep_on();
void beep_off();
void led_on();
void led_off();

#endif /* APPLICATIONS_HAL_H_ */

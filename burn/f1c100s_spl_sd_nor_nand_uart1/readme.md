### boot 说明

1. 本boot 适用于F1CX00系列启动rtthread
2. 本boot 支持内存启动、SD卡启动、nor-flash启动  nand-flash 驱动【nand flash 烧录问题 能启动boot 能读取数据，但是烧录会擦除 还未解决】
3. 本boot 优先尝试SD卡启动，SD卡无文件后，会尝试内存启动，在尝试Flash启动，从0x00010000 1M 区域开始校验数据
4. Flash校验数据为 [1] 0xaa55aa55 [2]0 [3]image_size
5. rtthread.bin 头部需要

修改链接文件 还在启动文件头部添加

```
b   system_vectors
.long 0xaa55aa55
.long 0
.long image_size
```
在启动文件中添加标识 就可以使用在主线RTT上了



nandflash 使用xfel 工具烧录   见 https://gitee.com/xboot/xfel



日志串口 如果需要切换   修改sys-uart.c  

默认使用 UART1  屏蔽 #define  PCB_UART1 则使用UART0 打印日志



关于SD 内存 norflash nandflash 修改sys-copyself.c 代码

本意是读取 0x0000058 去选择启动设备的 但是好像 并不可用 可以直接赋值使用自己需要的设备

这个是 arm-eabi-gcc
 https://releases.linaro.org/components/toolchain/binaries/5.3-2016.05/arm-eabi/gcc-linaro-5.3.1-2016.05-x86_64_arm-eabi.tar.xz
这里是 arm-none-eabi-gcc

还有一个是 /spl/SConscript 下倒数几行 env.AddPostAction('spl.elf', 'mksunxi\\mksunxi boot0607.bin') windows 需要\\ 

### 编译

1. 注意修改 spl /SConscript 文件的  env.PrependENVPath('PATH', r'../f1c100s_rt-thread/tools/gcc-arm-none-eabi/bin')    为自己的 gcc 路径             

```
ubuntu18@ubuntu:~/Workspace/F1C200S/RTOS/f1c100s_spl_sd_nor_nand_uart$ scons
scons: Reading SConscript files ...
scons: done reading SConscript files.
scons: Building targets ...
gcc -o mksunxi/mksunxi.o -c mksunxi/mksunxi.c
gcc -o mksunxi/mksunxi mksunxi/mksunxi.o
arm-none-eabi-gcc -o spl/src/drv_clock.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/drv_clock.c
arm-none-eabi-gcc -o spl/src/drv_gpio.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/drv_gpio.c
arm-none-eabi-gcc -o spl/src/log.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/log.c
arm-none-eabi-gcc -o spl/src/sys-clock.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/sys-clock.c
arm-none-eabi-gcc -o spl/src/sys-copyself.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/sys-copyself.c
arm-none-eabi-gcc -o spl/src/sys-dram.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/sys-dram.c
arm-none-eabi-gcc -o spl/src/sys-sdio.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/sys-sdio.c
arm-none-eabi-gcc -o spl/src/sys-spi-flash.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/sys-spi-flash.c
arm-none-eabi-gcc -o spl/src/sys-spinand.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/sys-spinand.c
arm-none-eabi-gcc -o spl/src/sys-uart.o -c -mcpu=arm926ej-s -ffunction-sections -fdata-sections -Wall -O0 -Ispl/src spl/src/sys-uart.c
arm-none-eabi-gcc -mcpu=arm926ej-s -ffunction-sections -fdata-sections -c -x assembler-with-cpp -Ispl/src -c -o spl/src/start.o spl/src/start.S
arm-none-eabi-g++ -o spl/spl.elf -mcpu=arm926ej-s -ffunction-sections -fdata-sections -nostartfiles -Wl,--gc-sections,-Map=boot.map,-cref -Tspl/link.ld spl/src/drv_clock.o spl/src/drv_gpio.o spl/src/log.o spl/src/sys-clock.o spl/src/sys-copyself.o spl/src/sys-dram.o spl/src/sys-sdio.o spl/src/sys-spi-flash.o spl/src/sys-spinand.o spl/src/sys-uart.o spl/src/start.o
arm-none-eabi-objdump -D -S spl/spl.elf > boot.asm
arm-none-eabi-objcopy -O binary spl/spl.elf boot0607.bin
arm-none-eabi-size spl/spl.elf
   text	   data	    bss	    dec	    hex	filename
  21202	     24	 524328	 545554	  85312	spl/spl.elf
./mksunxi/mksunxi boot0607.bin
Input file size 21232
after align size 21504
size in file 21232
after align size 21504
The bootloader head has been fixed, spl size is 21504 bytes.
scons: done building targets.
ubuntu18@ubuntu:~/Workspace/F1C200S/RTOS/f1c100s_spl_sd_nor_nand_uart$
```

好像把代码的ROM启动给搞坏了

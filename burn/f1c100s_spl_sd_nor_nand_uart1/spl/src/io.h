#ifndef __IO_H__
#define __IO_H__


typedef signed   char                   rt_int8_t;      /**<  8bit integer type */
typedef signed   short                  rt_int16_t;     /**< 16bit integer type */
typedef signed   int                    rt_int32_t;     /**< 32bit integer type */
typedef unsigned char                   rt_uint8_t;     /**<  8bit unsigned integer type */
typedef unsigned short                  rt_uint16_t;    /**< 16bit unsigned integer type */
typedef unsigned int                    rt_uint32_t;    /**< 32bit unsigned integer type */
typedef signed   int 					rt_err_t;

#define RT_EOK                          0               /**< There is no error */
#define RT_ERROR                        1               /**< A generic error happens */
#define RT_ETIMEOUT                     2               /**< Timed out */
#define RT_EFULL                        3               /**< The resource is full */
#define RT_EEMPTY                       4               /**< The resource is empty */
#define RT_ENOMEM                       5               /**< No memory */
#define RT_ENOSYS                       6               /**< No system */
#define RT_EBUSY                        7               /**< Busy */
#define RT_EIO                          8               /**< IO error */
#define RT_EINTR                        9               /**< Interrupted system call */
#define RT_EINVAL                       10              /**< Invalid argument */

#include <stdint.h>
#include "sys-sdio.h"

extern void sys_uart_putc(char c);
extern void return_to_fel(void);
extern void sys_uart_putc(char c);
extern void sys_spi_flash_init(void);
extern void sys_spi_flash_exit(void);
extern int  sys_spi_flash_read(int addr, void * buf, int count);
extern void sys_spi_flash_deinit(void);
extern void sys_spi_flash_init2(void);
extern int logout(const char *fmt, ...);
extern void print_string(const char *s);
extern void print_hex(uint32_t num);

#define NULL    ((void*)0)

static inline uint8_t read8(uint32_t addr){
	return( *((volatile uint8_t *)(addr)) );
}

static inline uint32_t read32(uint32_t addr){
	return( *((volatile uint32_t *)(addr)) );
}

static inline void write8(uint32_t addr, uint8_t value){
	*((volatile uint8_t *)(addr)) = value;
}

static inline void write32(uint32_t addr, uint32_t value){
	*((volatile uint32_t *)(addr)) = value;
}

#endif

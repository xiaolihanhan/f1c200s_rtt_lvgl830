#  Allwinner tina板级支持包

## 1. 简介

Allwinner tina 是由全志公司推出的ARM9内核的SOC 适用的开发板原理图见 burn/CherryPi-F1C200S.pdf

burn/boot.bin 来源自 https://gitee.com/LicheePiNano/lv7_rtthread_f1c100s?_from=gitee_search 需要源码自行下载编译即可，这个boot.bin 支持内存启动和norflash启动，由于spi 速率设置为100，也可能norflash 不支持 100M 启动会异常

我自己改造的boot 在 burn/f1c100s_spl_sd_nor_nand_uart1目录下，根据目标板，将boot log 输出到串口1,也可以修改成uart0, 屏蔽掉宏定义即可，我将spi 速度降为50M了，如果自己的nor flash 支持更改可以修改成100M， nand flash 驱动可以读取数据，但是卡在烧录这块 导致没启动成功，其他方式都可以启动成功

包括如下硬件特性：

| 硬件               | 描述    |
| ------------------ | ------- |
| 芯片型号           | F1C100S |
| CPU                | ARM9    |
| 主频               | 408MHz  |
| 片内DDR            | 32MB    |
| 板载SPI Nand Flash | 128MB   |

## 2. 编译说明

| 环境       | 说明                                               |
| ---------- | -------------------------------------------------- |
| PC操作系统 | Windows                                            |
| 编译器     | arm-none-eabi-gcc version 6.3.1 20170620 (release) |
| 构建工具   | scons                                              |

1. 下载源码

```
    git clone https://gitee.com/kasira/f1c200s_rtt_lvgl830.git
```

1. 使用RT-Thread Studio 导入项目

```
    可以参考 https://whycan.com/t_9695.html 我写的说明 里面也包含 调试过程中遇得到的一些问题和解决方法
```

1. 编译安装下载工具

```
   
```

1. 编译项目

```
    工具栏选择 项目-》构建项目
```

如果编译正确无误，在Debug会产生rtthread.elf、rtthread.bin文件。其中rtthread.bin需要烧写到设备中进行运行。

## 3. 烧写及执行

1)下载并运行

```
1.连接USB
2.先按住开发板上的boot按钮再按下RST按钮
3.松开
4.双击burn目录下flashtoram.bat 烧录到内存运行，如果需要烧录到其他地方，可自行编译burn/f1c100s_spl_sd_nor_nand_uart1 下的 boot代码

D:\RT-ThreadStudio\workspace\iboard\burn>.\tools\sunxi-fel.exe spl boot.bin
D:\RT-ThreadStudio\workspace\iboard\burn>.\tools\sunxi-fel.exe -p write 0x80000
00 ..\Debug\rtthread.bin
100% [================================================]   440 kB,  309.2 kB/s
D:\RT-ThreadStudio\workspace\iboard\burn>.\tools\sunxi-fel.exe exec 0x80000000
请按任意键继续. . .
```

### 3.1 运行结果

如果编译 & 烧写无误，会在串口1上看到RT-Thread的启动logo信息：

如果您的开发板是CherryPi-F1C200S 直接使用开发板上的串口就行 i2c test CFG_VER GT911 verson 证明gt911 触摸OK last_x =533, last_y=368 触摸读出的坐标

```
 \ | /
- RT -     Thread Operating System
 / | \     4.1.0 build Jun  3 2023 16:37:03
 2006 - 2022 Copyright by RT-Thread team
[33m[W/SFUD] [SFUD] Warning: The OS tick(100) is less than 1000. So the flash write will take more time.[0m
[32m[I/SFUD] Warning: Read SFDP parameter header information failed. The gd25qxx is not support JEDEC SFDP.[0m
[32m[I/SFUD] Warning: This flash device is not found or not support.[0m
[32m[I/SFUD] Error: gd25qxx flash device is initialize fail.[0m
[31m[E/SFUD] ERROR: SPI flash probe failed by SPI device spi00.[0m
[32m[I/I2C] I2C bus [i2c0] registered[0m
i2c test CFG_VER:B
GT911 verson: 911_1060
periph_get_pll_clk:600000000
cpu_get_clk:408000000
ahb_get_clk:200000000
apb_get_clk:100000000
clk_divider_set_rate 29491216 198000000 0clk_divider_set_rate 2147040808 198000000 0
 ui_init 
msh />[31m[E/MMC] [err]:0x00000100,  RTO[0m
[31m[E/MMC] mmc cmd 8 err[0m
[31m[E/MMC] [err]:0x00000100,  RTO[0m
[31m[E/MMC] mmc cmd 5 err[0m
[31m[E/MMC] [err]:0x00000100,  RTO[0m
[31m[E/MMC] mmc cmd 55 err[0m
[31m[E/MMC] [err]:0x00000100,  RTO[0m
[31m[E/MMC] mmc cmd 55 err[0m
last_x =533, last_y=368 
last_x =533, last_y=368 
last_x =533, last_y=368 
last_x =533, last_y=368 
```

## 4. 驱动支持情况及计划

| 驱动  | 支持情况 | 备注                       |
| ----- | -------- | -------------------------- |
| UART  | 支持     | UART 1                     |
| GPIO  | 支持     | /                          |
| clock | 支持     | /                          |
| mmu   | 支持     | /                          |
| fb    | 支持     | 480*800                    |
| touch | 支持     | 触摸适配的可能是1024*600的 |

### 4.1 IO在板级支持包中的映射情况

| IO号 | 板级包中的定义 |
| ---- | -------------- |
| PE8  | USART2 RX      |
| PE7  | USART2 TX      |
| PA3  | USART1 RX      |
| PA2  | USART1 TX      |
| PE1  | USART0 TX      |
| PE0  | USART0 RX      |

## 5. 联系人信息

维护人:

微信号： loongfour

邮箱：[mitcub@163.com](mailto:mitcub@163.com)

```
Init OK
Boot to SPI mode
Copy Flash offset 64 to RAM 0x8000000 size:2505724
Copy Flash Ok!
Goto 0x80000000 ...

??/?initialize rti_board_start:0 done
initialize rt_hw_spi_init:0 done
initialize rt_hw_wdt_initwatchdog_init
:0 done

 \ | /
- RT -     Thread Operating System
 / | \     4.0.3 build May 17 2023
 2006 - 2020 Copyright by rt-thread team
do components initialization.
initialize rti_board_end:0 done
initialize rt_hw_spi_flash_with_sfud_init[32m[I/SFUD] Find a Winbond flash chip. Size is 16777216 bytes.[0m
[32m[I/SFUD] gd25qxx flash device is initialize success.[0m
[32m[I/SFUD] Probe SPI flash gd25qxx by SPI device spi00 success.[0m
:0 done
initialize dfs_init:0 done
initialize rt_mmcsd_core_init:0 done
initialize rt_hw_lcd_init:0 done
initialize rt_hw_gpio_init:0 done
initialize rt_hw_i2c_init[32m[I/I2C] I2C bus [i2c0] registered[0m
:0 done
initialize gt9xx_driver_registeri2c test failed attempt 1: 200i2c test failed attempt 2: 200[TP] gt9xx_probe goodix_i2c_test!  error
:0 done
initialize rt_work_sys_workqueue_init:0 done
initialize clock_time_system_init:0 done
initialize pthread_system_init:0 done
initialize libc_system_init:0 done
initialize sal_init[32m[I/sal.skt] Socket Abstraction Layer initialize success.[0m
:0 done
initialize cplusplus_system_init:0 done
initialize rt_i2c_core_init:0 done
initialize tina_sdio_init:0 done
initialize main_page_initclk_divider_set_rate 29491216 198000000 0clk_divider_set_rate -2147483648 198000000 0:0 done
initialize finsh_system_init:0 done
periph_get_pll_clk:600000000
cpu_get_clk:408000000
ahb_get_clk:200000000
apb_get_clk:100000000
msh />
```

### boot.bin 说明及RTT改造

boot.bin 合法格式。

f1c100s 对 spi 引导程序是有格式要求的。格式细节我忽略不分析 start.S 内定义。 

编译生成 boot.bin 之后用 mksunxi 对其进行校验，并填充相关位置。 让 f1c100s 能够认可 boot.bin ，并执行它。

burn/f1c100s_spl_sd_nor_nand_uart1 下的boot 在编译时会自动mksunxi 校验 



boot.bin 逻辑

初始化 CPU 寄存器。 初始化中断状态。 设置中断向量表位置。 赋值中断向量表。 初始化时钟、DRAM、串口。 bl sys_clock_init bl sys_dram_init bl sys_uart_init 读取第二段程序并引导启动。 这里，第二段程序是 RTT。 如果是 uboot ，第二段就是 uboot 第二阶段。 bl sys_copyself 结束 有三种结束情况 1.返回spl 2.启动第二段程序 3.死循环 具体分析 sys_copyself 函数。 获取启动方式，如果不是SPI，那就返回 spl 状态（start.S 内定义）。 从 spi flash 0x00010000 读取 16 字节。

```
struct { 
    void (*Exe)(void); // 程序地址？ 
    uint32_t magic; // 魔数 0xaa55aa55 
    uint32_t rev; // 没有使用。 
    uint32_t imgLength; // 程序大小。 
}head_t;
```

```
如果魔数不正确，将进入死循环 while(1)。
正确的情况下。
从 spi flash 0x00010000 读取 imgLength 长度数据到 0x80000000(DRAM);
然后直接跳转到 0x80000000。运行。
```

对应的第二阶段程序就有需要完成 head_t 头部信息。 

RTT 需要修改内容： 

start_gcc.S 文件 .vectors 

最前面加入 head_t 结构。 

b system_vectors ; 第二段程序向量表存放位置。 

.long 0xaa55aa55 ; 魔数

 .long 0 ; 留空

 .long image_size ; 

程序大小 image_size 让链接器进行计算即可，不需要另外使用工具进行处理。 

修改 link.lds 文件 最前面位置加入

 image_start = .; 

.bss 段前面加入

__ image_end = .; 

最末尾处计算一下

__ image_size PROVIDE(image_size = image_end - image_start);